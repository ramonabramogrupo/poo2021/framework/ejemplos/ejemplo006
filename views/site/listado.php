<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>

<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">Listado de peliculas</h1>

<?= GridView::widget([
    "dataProvider" => $dataProvider,
    "layout"=>"{items}<br>{pager}<br>{summary}",
    "pager"=>[
        "options" => ["class"=>"row col-6"],
        "linkOptions"=>["class"=>"p-2 d-block w-100 h-100"],
        "disabledPageCssClass"=>"p-2 rounded d-block col-1 border",
        "activePageCssClass"=>"bg-light",
        "nextPageCssClass"=>"rounded d-block col-1 border",
        "prevPageCssClass"=>"rounded d-block col-1 border",
        "pageCssClass"=>"rounded d-block col-1 p-0 border",
    ],
    "columns"=>[
        'id',
        'titulo',
        [
            'attribute'=>'categoria',
            'label'=>'Categoria',
            'format'=>'raw',
            'value' => function($data){
                return Html::a(
                            $data->categoria,
                            ['site/vergrid',"id"=>$data->id],
                            ["class"=>'btn btn-info mx-auto d-block',"style"=>"width:100px"]
                        );
            } 
        ],
        [
            'attribute'=>'portada',
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
                $url = "@web/imgs/$data->portada";
                return Html::img($url,['style'=>'width:200px','class'=>'img-fluid mx-auto d-block']); 
            } 
        ],
        'fechaEstreno',
    ],
    "options"=>[
        "class" => "text-center"
    ]
]) ?>
