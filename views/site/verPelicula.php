<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    "model" => $pelicula,
    "attributes"=>[
        'id',
        'titulo',
        'descripcion',
        'duracion',
        'fechaEstreno',
        'categoria',
        'director',
        'destacada',
        'peliculaSemana',
        [
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
                $url = "@web/imgs/$data->portada";
                return Html::img($url,['style'=>'width:200px']); 
            } 
        ],
        
        
    ]
    
]);
