<?php

/* @var $this yii\web\View */

$this->title = 'Peliculas';

use yii\helpers\Html;

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 text-justify">
               <p>
                   El cine es un arte y una técnica. Es el arte de narrar historias mediante la proyección de imágenes, de allí que también se lo conozca con el nombre de séptimo arte. Y es la técnica que consiste en proyectar fotogramas, de forma rápida y sucesiva, para crear la ilusión de movimiento.
            Para la realización del cine es necesaria la concurrencia de muchas otras capacidades a nivel técnico, creativo y financiero, como el montaje, la fotografía, la dirección, el guionismo, la operación de cámaras, el sonido, la producción, etc., para lo cual es necesario todo un equipo de trabajo. Asimismo, pasa por varias etapas: el desarrollo, la preproducción, el rodaje, la posproducción y la distribución.
               </p>
            </div>
            <div class="col-lg-5 ml-5 p-3">
                <h2 class="text-center border border-secondary bg-secondary text-white rounded p-3 mb-5">Pelicula de la semana</h2>
                
                <?= 
                Html::a(    
                            Html::img("@web/imgs/$pelicula",[
                                'class' => 'img-fluid d-block mx-auto' 
                            ])
                        ,["site/verpelicula","id"=>$id]
                        ,['class'=>'']
                        );
        
                ?>
                
            </div>
        </div>

    </div>
</div>
