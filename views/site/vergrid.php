<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>
<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">Listado de peliculas de Categoria <?= $categoria ?></h1>

<?php
echo GridView::widget([
    "dataProvider" => $dataProvider,
    "layout"=>"{items}<br>{pager}<br>{summary}",
    "pager"=>[
        "options" => ["class"=>"row col-6"],
        "linkOptions"=>["class"=>" d-block w-100 h-100"],
        "disabledPageCssClass"=>"rounded d-block col-1 border",
        "activePageCssClass"=>"bg-light",
        "nextPageCssClass"=>"rounded d-block col-1 border",
        "prevPageCssClass"=>"rounded d-block col-1 border",
        "pageCssClass"=>"rounded d-block col-1 p-0 border",
    ],
    "columns"=>[
        'id',
        'titulo',
        [
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
                $url = "@web/imgs/$data->portada";
                return Html::img($url,['style'=>'width:200px','class'=>'img-fluid mx-auto d-block']); 
            } 
        ],
        'fechaEstreno',
    ],
     "options"=>[
        "class" => "text-center"
    ]                
]);