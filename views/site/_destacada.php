<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-5">
    <?php
    echo Html::img("@web/imgs/$model->portada",[
    "class" =>"card-img-top"
    ]);
    
    ?>
    </div>
    <div class="col-lg-7">
        <h2><?= $model->titulo ?><br></h2>
        <div class="bg-warning rounded p-2">Duracion:</div>
        <div class="p-1"><?= $model->duracion ?> minutos</div>
        <div class="bg-warning rounded p-2">Director:</div>
        <div class="p-1"><?= $model->director ?></div>
        <div class="bg-warning rounded p-2">Categoria:</div>
        <div class="p-1"><?= $model->categoria ?></div>
        <?=  
           Html::a("Ver mas..",
        ['site/verpelicula','id'=>$model->id],
        ['class'=>'btn btn-primary float-right']
        );
            
         ?><br>
    </div>
    
</div>



