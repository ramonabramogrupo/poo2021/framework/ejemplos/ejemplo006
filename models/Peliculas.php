<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $descripcion
 * @property int|null $duracion
 * @property string|null $fechaEstreno
 * @property string $categoria
 * @property string $portada
 * @property string|null $director
 * @property int|null $destacada
 * @property int|null $peliculaSemana
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'titulo', 'categoria', 'portada'], 'required'],
            [['id', 'duracion', 'destacada', 'peliculaSemana'], 'integer'],
            [['fechaEstreno'], 'safe'],
            [['titulo'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255],
            [['categoria', 'portada', 'director'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'duracion' => 'Duracion',
            'fechaEstreno' => 'Fecha de Estreno',
            'categoria' => 'Categoria',
            'portada' => 'Portada',
            'director' => 'Director',
            'destacada' => 'Destacada',
            'peliculaSemana' => 'Pelicula Semana',
        ];
    }
}
